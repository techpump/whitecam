<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Application\DataTransformer;

interface DataTransformer
{
    /**
     * @param mixed $data
     * @return mixed
     */
    public function transform($data);
}