<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Configuration;

use Symfony\Component\Yaml\Yaml;
use Exception;
use TECHPUMP\WhiteCam\Shared\Domain\Services\Configuration\ConfigurationLoader;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Exception\Configuration\ClientConfigurationNotFound;

class ClientConfigurationLoader implements ConfigurationLoader
{
    private array $properties;
    private string $projectDir;
    private string $env;

    /**
     * @param string $projectDir
     * @param string $env
     */
    public function __construct(string $projectDir, string $env = 'prod')
    {
        $this->projectDir = $projectDir;
        $this->env = $env;
    }

    /**
     * @param string $domain
     * @throws ClientConfigurationNotFound
     */
    public function loadProperties(string $domain): void
    {
        $client = $this->client($domain);
        $this->properties = [];
        try {
            $this->properties = Yaml::parseFile($this->projectDir . '/config/clients/prod/' . $client . '.yaml', 2048);

            $envFileName = $this->projectDir . '/config/clients/' . $this->env . '/' . $client . '.yaml';

            if (file_exists($envFileName)) {
                $this->properties = array_merge(
                    $this->properties,
                    Yaml::parseFile($envFileName, 1)
                );
            }
        } catch (Exception $e) {
            ClientConfigurationNotFound::throw();
        }
    }

    /**
     * @param string $propertyKey
     * @return mixed
     */
    public function get(string $propertyKey)
    {
        return $this->properties[$propertyKey] ?? null;
    }

    /**
     * @param string $domain
     * @return string
     */
    public function client(string $domain): string
    {
        if (empty($domain)) {
            return '';
        }

        $client = trim($domain, '/');

        if (!preg_match('#^http(s)?://#', $client)) {
            $client = 'http://' . $client;
        }

        $urlParts = parse_url($client);

        $client = preg_replace('/^www\./', '', $urlParts['host']);

        //TODO: detectar subdominios para realizar esta comprobación
        $client = explode(".", $client)[0];

        return $client;
    }
}