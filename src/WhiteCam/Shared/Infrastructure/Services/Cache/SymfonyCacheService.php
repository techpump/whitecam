<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Cache;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;

abstract class SymfonyCacheService
{
    /** @var FilesystemAdapter */
    protected FilesystemAdapter $cache;

    /**
     * SymfonyCacheServiceReader constructor.
     * @param string $namespace
     * @param int $lifeTime
     */
    public function __construct(string $namespace = '', int $lifeTime = 0)
    {
        if(empty($this->cache)) {
            $this->cache = new FilesystemAdapter($namespace, $lifeTime);
        }
    }
}