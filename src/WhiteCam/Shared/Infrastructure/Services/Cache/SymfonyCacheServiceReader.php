<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Cache;

use Psr\Cache\InvalidArgumentException;
use TECHPUMP\WhiteCam\Shared\Domain\Services\Cache\CacheServiceReader;

class SymfonyCacheServiceReader extends SymfonyCacheService implements CacheServiceReader
{
    /**
     * SymfonyCacheServiceReader constructor.
     * @param string $namespace
     */
    public function __construct(string $namespace)
    {
        parent::__construct($namespace);
    }

    /**
     * @param string $key
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function execute(string $key)
    {
        return $this->cache->getItem($key)->get();
    }

}