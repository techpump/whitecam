<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class HttpClient
{
    /**
     * @var Client
     */
    private Client $client;

    /**
     * HttpClient constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $method HTTP method.
     * @param string $uri URI object or string.
     * @param array $options Request options to apply. See \GuzzleHttp\RequestOptions.
     * @return HttpClientResponse
     * @throws GuzzleException
     */
    public function request(string $method, string $uri = '', array $options = []): HttpClientResponse
    {
        $clientResponse = $this->client->request($method, $uri, $options);

        return new HttpClientResponse($clientResponse->getStatusCode(), $clientResponse->getBody()->getContents());
    }
}