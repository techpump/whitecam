<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Http;

class HttpClientResponse
{
    private int $statusCode;
    private string $body;

    /**
     * HttpClient constructor.
     * @param $statusCode
     * @param $body
     */
    public function __construct(int $statusCode, string $body)
    {
        $this->statusCode = $statusCode;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}