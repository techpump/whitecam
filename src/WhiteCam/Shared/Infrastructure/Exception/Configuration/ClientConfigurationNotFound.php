<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Infrastructure\Exception\Configuration;

use Exception;

class ClientConfigurationNotFound extends Exception
{
    /**
     * @throws static
     */
    public static function throw(): void
    {
        throw new static('No client configuration was found', 404);
    }
}