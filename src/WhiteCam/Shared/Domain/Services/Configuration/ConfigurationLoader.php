<?php


namespace TECHPUMP\WhiteCam\Shared\Domain\Services\Configuration;

interface ConfigurationLoader
{
    /**
     * @param string $propertyKey
     * @return mixed
     */
    public function get(string $propertyKey);
}