<?php


namespace TECHPUMP\WhiteCam\Shared\Domain\Services\Cache;


interface CacheServiceReader
{
    public function execute(string $key);
}