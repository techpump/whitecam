<?php


namespace TECHPUMP\WhiteCam\Shared\Domain\Services\Cache;


interface CacheServiceWriter
{
    public function execute(string $key, $object);
}