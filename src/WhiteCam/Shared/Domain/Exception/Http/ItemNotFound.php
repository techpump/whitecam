<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Domain\Exception\Http;

interface ItemNotFound
{
    public static function throw(): void;
}