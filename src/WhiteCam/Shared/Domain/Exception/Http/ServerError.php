<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Domain\Exception\Http;

interface ServerError
{
    public static function throw(): void;
}