<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Shared\Domain\ValueObject;

abstract class StringValueObject
{
    /**
     * @var string|null
     */
    protected ?string $value;

    /**
     * StringValueObject constructor.
     * @param string|null $value
     */
    protected function __construct(?string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string|null
     */
    public function value(): ?string
    {
        return $this->value;
    }

    /**
     * @return string|null
     */
    public function __toString(): ?string
    {
        return $this->value();
    }

    /**
     * @param string|null $string
     * @return static|null
     */
    public static function fromString(?string $string): ?self
    {
        if (is_null($string)) {
            return null;
        }

        return new static($string);
    }
}
