<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Cam\Domain\Entity;

use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Id;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Live;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Permalink;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Thumb;
use TECHPUMP\WhiteCam\Shared\Domain\ValueObject\Nick;

class Cam
{
    private Id $id;
    private Live $live;
    private Permalink $permalink;
    private array $thumbs;
    private Nick $nick;

    /**
     * Cam constructor.
     * @param Id $id
     * @param Live $live
     * @param Permalink $permalink
     * @param array $thumbs
     * @param Nick $nick
     */
    public function __construct(Id $id, Live $live, Permalink $permalink, array $thumbs, Nick $nick)
    {
        $this->id = $id;
        $this->live = $live;
        $this->permalink = $permalink;
        $this->thumbs = $thumbs;
        $this->nick = $nick;
    }

    /**
     * @param Id $id
     * @param Live $live
     * @param Permalink $permalink
     * @param array $thumbs
     * @param Nick $nick
     * @return static
     */
    public static function instantiate(Id $id, Live $live, Permalink $permalink, array $thumbs, Nick $nick): self
    {
        return new static($id, $live, $permalink, $thumbs, $nick);
    }

    /**
     * @return Id
     */
    public function id(): Id
    {
        return $this->id;
    }

    /**
     * @return Live
     */
    public function live(): Live
    {
        return $this->live;
    }

    /**
     * @return Permalink
     */
    public function permalink(): Permalink
    {
        return $this->permalink;
    }

    /**
     * @return array|Thumb[]
     */
    public function thumbs(): array
    {
        return $this->thumbs;
    }

    /**
     * @return Nick
     */
    public function nick(): Nick
    {
        return $this->nick;
    }


}
