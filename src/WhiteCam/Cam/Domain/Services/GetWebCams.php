<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Cam\Domain\Services;

use TECHPUMP\WhiteCam\Cam\Domain\Entity\Cam;

interface GetWebCams
{
    /**
     * @return Cam[]
     */
    public function execute(): array;
}