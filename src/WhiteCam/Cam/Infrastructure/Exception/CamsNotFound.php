<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Cam\Infrastructure\Exception;

use Exception;
use TECHPUMP\WhiteCam\Shared\Domain\Exception\Http\ItemNotFound;

final class CamsNotFound extends Exception implements ItemNotFound
{
    /**
     * @throws static
     */
    public static function throw(): void
    {
        throw new static('Cam list not found', 404);
    }
}