<?php

declare(strict_types=1);


namespace TECHPUMP\WhiteCam\Cam\Infrastructure\EntryPoint\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TECHPUMP\WhiteCam\Cam\Application\Command\GetCamList;
use TECHPUMP\WhiteCam\Cam\Application\Command\Handler\GetCamListHandler;
use TECHPUMP\WhiteCam\Cam\Application\DataTransformer\CamDataTransformer;
use TECHPUMP\WhiteCam\Cam\Domain\Services\GetWebCams;
use TECHPUMP\WhiteCam\Shared\Domain\Services\Configuration\ConfigurationLoader;

class CamListViewer extends AbstractController
{

    /**
     * @param Request $request
     * @param GetWebCams $listCamsService
     * @param CamDataTransformer $dataTransformerCam
     * @param ConfigurationLoader $configurationLoader
     * @return Response
     */
    public function __invoke(
        Request $request,
        GetWebCams $listCamsService,
        CamDataTransformer $dataTransformerCam,
        ConfigurationLoader $configurationLoader
    ) {

        try {
            $configurationLoader->loadProperties($request->getHttpHost());
        } catch (Exception $exception) {
            return $this->renderException($exception);
        }

        try {
            $camListHandler = new GetCamListHandler($listCamsService);
            $getCamListCommand = new GetCamList();

            $data = $camListHandler->handle($getCamListCommand);
        } catch (Exception $exception) {
            if ($exception->getCode() !== 404) {
                return $this->renderException($exception);
            }

            return $this->redirect($configurationLoader->get('client_404_redirect'), 301);
        }

        $numblocks = count($data) / 12;

        if (is_float($numblocks)) {
            $numblocks = intval($numblocks + 1);
        }

        $blocks = [];

        $counterAddedCams = 0;
        $bigThumbOrientation = 'izquierda';

        $bigThumbsCams = array_slice($data, 0, $numblocks);
        $smallerThumbsCams = array_slice($data, 1, count($data));

        $bigThumbPositionInBlock = 5;
        $totalCamsInBlock = 11;

        for ($i = 0; $i <= $numblocks; $i++) {
            $blockDataCams = array_slice($smallerThumbsCams, $counterAddedCams, $totalCamsInBlock);

            if (count($smallerThumbsCams) - $counterAddedCams > 5) {
                $firstBlock = array_slice($blockDataCams, 0, $bigThumbPositionInBlock);
                $secondBlock = array_slice($blockDataCams, $bigThumbPositionInBlock, count($blockDataCams));

                /* insert big thumb in camList */
                $blockDataCams = array_merge($firstBlock + [5 => $bigThumbsCams[$i]], $secondBlock);
            }

            $blocks[$i] = [
                'blockCamsList' => $blockDataCams,
                'bigThumbPosition' => $bigThumbOrientation
            ];

            $bigThumbOrientation = $bigThumbOrientation == 'izquierda' ? 'derecha' : 'izquierda';
            $counterAddedCams += $totalCamsInBlock;
        }


        return $this->render(
            'Cam/listCamViewer.html.twig',
            [
                'blocks' => $blocks,
                'clientName' => $configurationLoader->get('client_name'),
                'clientPublicDirectory' => $configurationLoader->get('client_public_directory_name'),
                'clientURL' => $configurationLoader->get('client_url'),
                'clientThumbsURL' => $configurationLoader->get('client_thumbs_directory'),
                'clientNatsWebcamsTracking' => $configurationLoader->get('client_nats_tracking_webcams'),
                'clientJoinPageURL' => $configurationLoader->get('client_join_page_url')
            ]
        );
    }

    /**
     * @param $exception
     * @return Response
     */
    private function renderException($exception): Response
    {
        return $this->render(
            'Exception/exception.html.twig',
            [
                'msg' => $exception->getMessage(),
            ]
        );
    }
}