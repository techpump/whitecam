<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Cam\Infrastructure\Services;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Exception;
use TECHPUMP\WhiteCam\Cam\Application\DataTransformer\CamDataTransformer;
use TECHPUMP\WhiteCam\Cam\Domain\Entity\Cam;
use TECHPUMP\WhiteCam\Cam\Domain\Services\GetWebCams;
use TECHPUMP\WhiteCam\Cam\Infrastructure\Exception\CamsNotFound;
use TECHPUMP\WhiteCam\Shared\Domain\Services\Cache\CacheServiceReader;
use TECHPUMP\WhiteCam\Shared\Domain\Services\Cache\CacheServiceWriter;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Http\HttpClient;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Http\HttpClientResponse;

class CumlouderGetWebCams implements GetWebCams
{
    private CamDataTransformer $dataTransformer;
    private HttpClient $httpClient;
    private string $urlService;

    private CacheServiceReader $cacheServiceReader;
    private CacheServiceWriter $cacheServiceWriter;

    /**
     * CumlouderGetWebCams constructor.
     * @param CamDataTransformer $dataTransformer
     * @param HttpClient $httpClient
     * @param string $urlService
     * @param CacheServiceReader $cacheServiceReader
     * @param CacheServiceWriter $cacheServiceWriter
     */
    public function __construct(
        CamDataTransformer $dataTransformer,
        HttpClient $httpClient,
        string $urlService,
        CacheServiceReader $cacheServiceReader,
        CacheServiceWriter $cacheServiceWriter
    ) {
        $this->urlService = $urlService;
        $this->dataTransformer = $dataTransformer;
        $this->httpClient = $httpClient;
        $this->cacheServiceReader = $cacheServiceReader;
        $this->cacheServiceWriter = $cacheServiceWriter;
    }

    /**
     * @return Cam[]
     * @throws Exception|GuzzleException
     */
    public function execute(): array
    {
        $cams = [];

        try {
            if (!empty($this->cacheServiceReader->execute('dataListCamService'))) {
                return $this->cacheServiceReader->execute('dataListCamService');
            }

            $serviceResponse = $this->get($this->urlService);
            $response = json_decode($serviceResponse->getBody(), true);

            if (empty($response)) {
                CamsNotFound::throw();
            }

            foreach ($response as $cam) {
                $cams[] = $this->dataTransformer->transform($cam);
            }

            $this->cacheServiceWriter->execute('dataListCamService', $cams);
        } catch (ClientException $exception) {
            $this->manageException($exception);
        }
        return $cams;
    }

    /**
     * @param string $endPoint
     * @param array $parameters
     * @return HttpClientResponse
     * @throws GuzzleException
     */
    public function get(string $endPoint, array $parameters = []): HttpClientResponse
    {
        $parametersString = !empty($parameters) ? '?' . http_build_query($parameters) : '';
        $uri = $endPoint . $parametersString;

        return $this->httpClient->request('GET', $uri);
    }

    /**
     * @param Exception $exception
     * @throws Exception
     */
    protected function manageException(Exception $exception): void
    {
        $code = $exception->getCode();

        if ($code == 404) {
            CamsNotFound::throw();
        }

        throw $exception;
    }
}