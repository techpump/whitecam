<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Cam\Application\Command\Handler;

use TECHPUMP\WhiteCam\Cam\Application\Command\GetCamList;
use TECHPUMP\WhiteCam\Cam\Domain\Entity\Cam;
use TECHPUMP\WhiteCam\Cam\Domain\Services\GetWebCams;

class GetCamListHandler
{
    private GetWebCams $getWebCamsService;

    /**
     * GetCamListHandler constructor.
     * @param GetWebCams $getWebCamsService
     */
    public function __construct(GetWebCams $getWebCamsService)
    {
        $this->getWebCamsService = $getWebCamsService;
    }

    /**
     * @param GetCamList $command
     * @return array|Cam[]
     */
    public function handle(GetCamList $command): array
    {
        return $this->getWebCamsService->execute();
    }
}