<?php

declare(strict_types=1);

namespace TECHPUMP\WhiteCam\Cam\Application\DataTransformer;

use TECHPUMP\WhiteCam\Cam\Domain\Entity\Cam;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Id;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Live;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Permalink;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Thumb;
use TECHPUMP\WhiteCam\Shared\Application\DataTransformer\DataTransformer;
use TECHPUMP\WhiteCam\Shared\Domain\ValueObject\Nick;

class CamDataTransformer implements DataTransformer
{
    public const KEY_CAM_ID = 'wbmerId';
    public const KEY_CAM_PERMALINK = 'wbmerPermalink';
    public const KEY_CAM_LIVE = 'wbmerLive';
    public const KEY_CAM_THUMB1 = 'wbmerThumb1';
    public const KEY_CAM_THUMB2 = 'wbmerThumb2';
    public const KEY_CAM_THUMB3 = 'wbmerThumb3';
    public const KEY_CAM_THUMB4 = 'wbmerThumb4';
    public const KEY_CAM_NICK = 'wbmerNick';


    public function transform($data): Cam
    {
        if (!isset($data)) {
            //TODO: return exception
        }

        return Cam::instantiate(
            Id::fromString($data[self::KEY_CAM_ID]),
            Live::fromString($data[self::KEY_CAM_LIVE]),
            Permalink::fromString($data[self::KEY_CAM_PERMALINK]),
            [
                Thumb::fromString($data[self::KEY_CAM_THUMB1]),
                Thumb::fromString($data[self::KEY_CAM_THUMB2]),
                Thumb::fromString($data[self::KEY_CAM_THUMB3]),
                Thumb::fromString($data[self::KEY_CAM_THUMB4]),
            ],
            Nick::fromString($data[self::KEY_CAM_NICK])
        );
    }
}