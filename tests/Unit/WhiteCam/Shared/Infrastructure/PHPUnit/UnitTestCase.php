<?php

namespace TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\PHPUnit;

use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

class UnitTestCase extends TestCase
{
    /**
     * @param string $className
     * @return MockInterface
     */
    protected function mock(string $className): MockInterface
    {
        return Mockery::mock($className);
    }
}