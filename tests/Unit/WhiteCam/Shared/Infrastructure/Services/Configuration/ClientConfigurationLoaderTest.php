<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\Services\Configuration;


use TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\PHPUnit\UnitTestCase;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Exception\Configuration\ClientConfigurationNotFound;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Configuration\ClientConfigurationLoader;

class ClientConfigurationLoaderTest extends UnitTestCase
{
    /**
     * @var ClientConfigurationLoader
     */
    private ClientConfigurationLoader $clientConfigurationLoader;

    protected function setUp(): void
    {
        $this->clientConfigurationLoader = new ClientConfigurationLoader('.', 'prod');
        parent::setUp();
    }

    public function testGetProperty()
    {

        $this->clientConfigurationLoader->loadProperties('http://www.conejox.com');
        $this->assertEquals('http://www.conejox.com', $this->clientConfigurationLoader->get('client_url'));
    }

    public function testGetPropertyDomainWithinHttp()
    {

        $this->clientConfigurationLoader->loadProperties('www.conejox.com');
        $this->assertEquals('http://www.conejox.com', $this->clientConfigurationLoader->get('client_url'));
    }

    public function testConfigurationFileNotFound()
    {
        $this->expectException(ClientConfigurationNotFound::class);
        $this->expectExceptionCode(404);

        $this->clientConfigurationLoader->loadProperties('');
    }
}