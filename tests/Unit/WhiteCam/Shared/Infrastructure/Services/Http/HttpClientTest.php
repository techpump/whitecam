<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\Services\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Http\HttpClient;
use TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\PHPUnit\UnitTestCase;

class HttpClientTest extends UnitTestCase
{
    public function testGetRequestResponseOkStatus200()
    {
        $mock = new MockHandler(
            [
                new Response(200, ['X-Foo' => 'Bar'], 'Ok')
            ]
        );

        $handlerStack = HandlerStack::create($mock);

        $client = new Client(['handler' => $handlerStack]);
        $client = new HttpClient($client);

        $response = $client->request('GET', '/');

        $this->assertEquals('Ok', $response->getBody());
        $this->assertEquals(200, $response->getStatusCode());
    }

    //TODO: implementar algún test más para excepciones
}
