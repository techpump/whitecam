<?php

declare(strict_types=1);


namespace TECHPUMP\Tests\Unit\WhiteCam\Cam\Domain\ValueObject;


use PHPUnit\Framework\TestCase;
use TECHPUMP\WhiteCam\Shared\Domain\ValueObject\StringValueObject;

class StringValueObjectTest extends TestCase
{
    public function testFromStringWithNull()
    {
        $this->assertNull(StringValueObject::fromString(null));
    }


}