<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Unit\WhiteCam\Cam\Domain\Entity;

use Exception;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject\IdMother;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject\LiveMother;
use TECHPUMP\Tests\Mother\WhiteCam\Shared\Domain\ValueObject\NickMother;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject\PermalinkMother;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject\ThumbMother;
use TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\PHPUnit\UnitTestCase;
use TECHPUMP\WhiteCam\Cam\Domain\Entity\Cam;

class CamTest extends UnitTestCase
{
    /**
     * @throws Exception
     */
    public function testInstantiateLiveCam()
    {
        $idCam = IdMother::random();
        $liveCam = LiveMother::isLive();
        $permalinkCam = PermalinkMother::random();
        $thumbsCam = [
            ThumbMother::random(),
            ThumbMother::random(),
            ThumbMother::random(),
            ThumbMother::random(),
        ];
        $nickCam = NickMother::random();
        $cam = Cam::instantiate(
            $idCam,
            $liveCam,
            $permalinkCam,
            $thumbsCam,
            $nickCam
        );

        $this->assertEquals($idCam, $cam->id()->value());
        $this->assertEquals($liveCam, $cam->live()->value());
        $this->assertEquals($permalinkCam, $cam->permalink()->value());
        $this->assertEquals($thumbsCam[0], $cam->thumbs()[0]);
        $this->assertEquals($thumbsCam[1], $cam->thumbs()[1]);
        $this->assertEquals($thumbsCam[2], $cam->thumbs()[2]);
        $this->assertEquals($thumbsCam[3], $cam->thumbs()[3]);
        $this->assertEquals($nickCam, $cam->nick()->value());
    }

    /**
     * @throws Exception
     */
    public function testInstantiateNotLiveCam()
    {
        $idCam = IdMother::random();
        $liveCam = LiveMother::isNotLive();
        $permalinkCam = PermalinkMother::random();
        $thumbsCam = [
            ThumbMother::random(),
            ThumbMother::random(),
            ThumbMother::random(),
            ThumbMother::random(),
        ];
        $nickCam = NickMother::random();
        $cam = Cam::instantiate(
            $idCam,
            $liveCam,
            $permalinkCam,
            $thumbsCam,
            $nickCam
        );

        $this->assertEquals($idCam, $cam->id()->value());
        $this->assertEquals($liveCam, $cam->live()->value());
        $this->assertEquals($permalinkCam, $cam->permalink()->value());
        $this->assertEquals($thumbsCam[0], $cam->thumbs()[0]);
        $this->assertEquals($thumbsCam[1], $cam->thumbs()[1]);
        $this->assertEquals($thumbsCam[2], $cam->thumbs()[2]);
        $this->assertEquals($thumbsCam[3], $cam->thumbs()[3]);
        $this->assertEquals($nickCam, $cam->nick()->value());
    }
}
