<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Unit\WhiteCam\Cam\Infrastructure\Services;


use GuzzleHttp\Exception\ServerException;
use TECHPUMP\Tests\Unit\WhiteCam\Cam\Infrastructure\PHPUnit\CamServicesUnitTestCase;
use TECHPUMP\WhiteCam\Cam\Application\DataTransformer\CamDataTransformer;
use TECHPUMP\WhiteCam\Cam\Infrastructure\Services\CumlouderGetWebCams;
use TECHPUMP\WhiteCam\Cam\Infrastructure\Exception\CamsNotFound;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Cache\SymfonyCacheServiceReader;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Cache\SymfonyCacheServiceWriter;
use TECHPUMP\WhiteCam\Shared\Infrastructure\Services\Http\HttpClient;

class CumlouderGetWebCamsTest extends CamServicesUnitTestCase
{
    private array $dataCam;
    private CamDataTransformer $dataTransformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->dataTransformer = new CamDataTransformer();

        $this->dataCam = [
            "wbmerTwitter" => "",
            "wbmerId" => "6723",
            "wbmerNick" => "Eva Madrid",
            "wbmerPermalink" => "eva-madrid",
            "wbmerOnline" => "1",
            "wbmerBirthdate" => "1983-01-01",
            "wbmerHeight" => "161",
            "wbmerCountry" => "AR",
            "wbmerSystem" => "1",
            "wbmerSystemId" => "8882",
            "wbmerThumb1" => "d9d7537ab72121783d1324b4c638af15.jpg",
            "wbmerThumb2" => "b883420611c7d31a312e67fc471b8b16.jpg",
            "wbmerThumb3" => "8208234bcbfb904c0ab6d8233cc6f123.jpg",
            "wbmerThumb4" => "c49cfaf5bf1903f883792dd953461e38.jpg",
            "wbmerActive" => "1",
            "wbmerLive" => "1",
            "wbmerNew" => "0",
            "wbmerBanned" => "0",
            "wbmerRanking" => "1",
            "wbmerLastLogin" => "2020-07-06 18:52:01",
            "wbmerRegister" => "2013-12-10",
            "wbmerPHd" => "1",
            "wbmerPAudio" => "0",
            "wbmerDtRate" => "9.91",
            "wbmerEspSwf" => null,
            "wbmerVideos" => "",
            "wbmerPositionOnline" => "202.81",
            "wbmerPosition" => "202.81",
            "wbmerSoft" => "0",
            "languages" => "ES"
        ];
    }

    public function testGetCumlouderWebCams()
    {
        $dataResponse = json_encode([$this->dataCam]);
        $httpClient = new HttpClient($this->getClient(200, [],  $dataResponse));
        $cacheServiceReader = new SymfonyCacheServiceReader('test');
        $cacheServiceWriter = new SymfonyCacheServiceWriter('test', 5);
        $service = new CumlouderGetWebCams($this->dataTransformer, $httpClient, 'http://webcams.cumlouder.com/feed/webcams/online/61/1/', $cacheServiceReader, $cacheServiceWriter);

        $responseService = $service->execute();

        $this->assertEquals($this->dataCam["wbmerId"], $responseService[0]->id()->value());
        $this->assertEquals($this->dataCam["wbmerLive"], $responseService[0]->live()->value());
        $this->assertEquals($this->dataCam["wbmerPermalink"], $responseService[0]->permalink()->value());
        $this->assertEquals($this->dataCam["wbmerThumb1"], $responseService[0]->thumbs()[0]);
        $this->assertEquals($this->dataCam["wbmerThumb2"], $responseService[0]->thumbs()[1]);
        $this->assertEquals($this->dataCam["wbmerThumb3"], $responseService[0]->thumbs()[2]);
        $this->assertEquals($this->dataCam["wbmerThumb4"], $responseService[0]->thumbs()[3]);
        $this->assertEquals($this->dataCam["wbmerNick"], $responseService[0]->nick()->value());
    }

    public function testCamsNotFound()
    {
        $this->expectException(CamsNotFound::class);
        $this->expectExceptionCode(404);

        $cacheServiceReader = new SymfonyCacheServiceReader('testCamsNotFound');
        $cacheServiceWriter = new SymfonyCacheServiceWriter('testCamsNotFound', 0);

        $httpClient = new HttpClient($this->getClient(404));
        $service = new CumlouderGetWebCams($this->dataTransformer, $httpClient, '', $cacheServiceReader, $cacheServiceWriter);

        $service->execute();
    }

    public function testUnexpectedException()
    {
        $this->expectExceptionCode(403);
        $cacheServiceReader = new SymfonyCacheServiceReader('testUnexpectedException');
        $cacheServiceWriter = new SymfonyCacheServiceWriter('testUnexpectedException', 0);
        $httpClient = new HttpClient($this->getClient(403));
        $service = new CumlouderGetWebCams($this->dataTransformer, $httpClient, '', $cacheServiceReader, $cacheServiceWriter);

        $service->execute();
    }

    public function testCamsInternalServerError()
    {
        $this->expectException(ServerException::class);
        $this->expectExceptionCode(500);
        $cacheServiceReader = new SymfonyCacheServiceReader('testCamsInternalServerError');
        $cacheServiceWriter = new SymfonyCacheServiceWriter('testCamsInternalServerError', 0);
        $httpClient = new HttpClient($this->getClient(500));
        $service = new CumlouderGetWebCams($this->dataTransformer, $httpClient, '', $cacheServiceReader, $cacheServiceWriter);

        $service->execute();
    }

    public function testCamsServiceUnavailable()
    {
        $this->expectException(ServerException::class);
        $this->expectExceptionCode(503);
        $cacheServiceReader = new SymfonyCacheServiceReader('testCamsServiceUnavailable');
        $cacheServiceWriter = new SymfonyCacheServiceWriter('testCamsServiceUnavailable', 0);
        $httpClient = new HttpClient($this->getClient(503));
        $service = new CumlouderGetWebCams($this->dataTransformer, $httpClient, '', $cacheServiceReader, $cacheServiceWriter);

        $service->execute();
    }

    public function testEmptyResponse()
    {
        $this->expectException(CamsNotFound::class);
        $this->expectExceptionCode(404);
        $cacheServiceReader = new SymfonyCacheServiceReader('testCamsServiceUnavailable');
        $cacheServiceWriter = new SymfonyCacheServiceWriter('testCamsServiceUnavailable', 0);
        $httpClient = new HttpClient($this->getClient(200, [], '[]'));
        $service = new CumlouderGetWebCams($this->dataTransformer, $httpClient, '', $cacheServiceReader, $cacheServiceWriter);

        $service->execute();
    }

    public function testReadCamsFromCache()
    {
        $cacheServiceWriter = new SymfonyCacheServiceWriter('testReadCamsFromCache',4);
        $cacheServiceReader = new SymfonyCacheServiceReader('testReadCamsFromCache');

        $dataResponse = json_encode([$this->dataCam]);
        $httpClient = new HttpClient($this->getClient(200, [],  $dataResponse));

        $service = new CumlouderGetWebCams($this->dataTransformer, $httpClient, 'http://webcams.cumlouder.com/feed/webcams/online/61/1/', $cacheServiceReader, $cacheServiceWriter);
        $service->execute();

        $service->execute();
        $this->assertNotNull($cacheServiceReader->execute('dataListCamService'));
    }

}