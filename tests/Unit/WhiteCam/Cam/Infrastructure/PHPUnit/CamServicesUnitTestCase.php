<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Unit\WhiteCam\Cam\Infrastructure\PHPUnit;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\PHPUnit\UnitTestCase;

class CamServicesUnitTestCase extends UnitTestCase
{

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @param int $responseCode
     * @param array $options
     * @param string $responseData
     * @return Client
     */
    public function getClient(int $responseCode, array $options = [], string $responseData = null)
    {
        $mock = new MockHandler([new Response($responseCode, $options, $responseData)]);

        $handlerStack = HandlerStack::create($mock);

        return new Client(['handler' => $handlerStack]);
    }
}
