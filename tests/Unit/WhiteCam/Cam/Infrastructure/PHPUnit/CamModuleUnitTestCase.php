<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Unit\WhiteCam\Cam\Infrastructure\PHPUnit;

use Mockery\MockInterface;
use TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\PHPUnit\UnitTestCase;
use TECHPUMP\WhiteCam\Cam\Domain\Services\GetWebCams;

class CamModuleUnitTestCase extends UnitTestCase
{
    protected GetWebCams $getWebCamsService;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function shouldGetCams(array $cams): void
    {
        $this->getWebCamsService()
            ->shouldReceive('execute')
            ->once()
            ->andReturn($cams);
    }

    /**
     * @return MockInterface|GetWebCams
     */
    protected function getWebCamsService(): MockInterface
    {
        if (empty($this->getWebCamsService)) {
            $this->getWebCamsService = $this->mock(GetWebCams::class);
        }
        return $this->getWebCamsService;
    }


}
