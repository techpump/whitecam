<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Unit\WhiteCam\Cam\Application\Command\Handler;

use Exception;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Application\Command\GetCamListMother;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\Entity\CamMother;
use TECHPUMP\Tests\Unit\WhiteCam\Cam\Infrastructure\PHPUnit\CamModuleUnitTestCase;
use TECHPUMP\WhiteCam\Cam\Application\Command\Handler\GetCamListHandler;
use TECHPUMP\WhiteCam\Cam\Domain\Entity\Cam;

class GetCamListHandlerTest extends CamModuleUnitTestCase
{
    private GetCamListHandler $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->handler = new GetCamListHandler($this->getWebCamsService());
    }

    /**
     * @throws Exception
     */
    public function testGetCamList()
    {
        /** @var Cam[] $cams */
        $cams = [
            CamMother::randomLiveCam(),
            CamMother::randomLiveCam()
        ];

        $this->shouldGetCams($cams);

        $command = GetCamListMother::create();
        $responseCams = $this->handler->handle($command);
        $this->assertEquals($cams[0]->id()->value(), $responseCams[0]->id()->value());
        $this->assertEquals($cams[1]->id()->value(), $responseCams[1]->id()->value());
    }



}