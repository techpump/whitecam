<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Unit\WhiteCam\Cam\Application\DataTransformer;

use TECHPUMP\Tests\Unit\WhiteCam\Shared\Infrastructure\PHPUnit\UnitTestCase;
use TECHPUMP\WhiteCam\Cam\Application\DataTransformer\CamDataTransformer;

class CamDataTransformerTest extends UnitTestCase
{
    private CamDataTransformer $dataTransformerCam;
    private array $dataCam;

    protected function setUp(): void
    {
        $this->dataTransformerCam = new CamDataTransformer();
        $this->dataCam = [
            "wbmerTwitter" => "",
            "wbmerId" => "6723",
            "wbmerNick" => "Eva Madrid",
            "wbmerPermalink" => "eva-madrid",
            "wbmerOnline" => "1",
            "wbmerBirthdate" => "1983-01-01",
            "wbmerHeight" => "161",
            "wbmerCountry" => "AR",
            "wbmerSystem" => "1",
            "wbmerSystemId" => "8882",
            "wbmerThumb1" => "d9d7537ab72121783d1324b4c638af15.jpg",
            "wbmerThumb2" => "b883420611c7d31a312e67fc471b8b16.jpg",
            "wbmerThumb3" => "8208234bcbfb904c0ab6d8233cc6f123.jpg",
            "wbmerThumb4" => "c49cfaf5bf1903f883792dd953461e38.jpg",
            "wbmerActive" => "1",
            "wbmerLive" => "1",
            "wbmerNew" => "0",
            "wbmerBanned" => "0",
            "wbmerRanking" => "1",
            "wbmerLastLogin" => "2020-07-06 18:52:01",
            "wbmerRegister" => "2013-12-10",
            "wbmerPHd" => "1",
            "wbmerPAudio" => "0",
            "wbmerDtRate" => "9.91",
            "wbmerEspSwf" => null,
            "wbmerVideos" => "",
            "wbmerPositionOnline" => "202.81",
            "wbmerPosition" => "202.81",
            "wbmerSoft" => "0",
            "languages" => "ES"
        ];
    }

    public function testTransformCam()
    {
        $cam = $this->dataTransformerCam->transform($this->dataCam);

        $this->assertEquals($this->dataCam["wbmerId"], $cam->id()->value());
        $this->assertEquals($this->dataCam["wbmerPermalink"], $cam->permalink()->value());
        $this->assertEquals($this->dataCam["wbmerLive"], $cam->live()->value());
        $this->assertEquals($this->dataCam["wbmerThumb1"], $cam->thumbs()[0]);
        $this->assertEquals($this->dataCam["wbmerThumb2"], $cam->thumbs()[1]);
        $this->assertEquals($this->dataCam["wbmerThumb3"], $cam->thumbs()[2]);
        $this->assertEquals($this->dataCam["wbmerThumb4"], $cam->thumbs()[3]);
        $this->assertEquals($this->dataCam["wbmerNick"], $cam->nick()->value());
    }
}