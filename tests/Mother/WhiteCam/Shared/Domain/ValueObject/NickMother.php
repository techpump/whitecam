<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Mother\WhiteCam\Shared\Domain\ValueObject;

use Faker\Factory;
use TECHPUMP\WhiteCam\Shared\Domain\ValueObject\Nick;
use Exception;

final class NickMother
{
    /**
     * @param string $nick
     * @return Nick
     * @throws Exception
     */
    public static function create(string $nick): Nick
    {
        return Nick::fromString($nick);
    }

    /**
     * @return Nick
     * @throws Exception
     */
    public static function random(): Nick
    {
        $faker = Factory::create();
        return self::create((string) $faker->name('female'));
    }
}
