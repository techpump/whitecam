<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\Entity;

use Exception;
use TECHPUMP\WhiteCam\Cam\Domain\Entity\Cam;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject\IdMother;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject\LiveMother;
use TECHPUMP\Tests\Mother\WhiteCam\Shared\Domain\ValueObject\NickMother;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject\PermalinkMother;
use TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject\ThumbMother;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Id;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Live;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Permalink;
use TECHPUMP\WhiteCam\Shared\Domain\ValueObject\Nick;

class CamMother
{
    /**
     * @param Id $idCam
     * @param Live $liveCam
     * @param Permalink $permalinkCam
     * @param array $thumbsCam
     * @param Nick $nickCam
     * @return Cam
     */
    public static function create(
        Id $idCam,
        Live $liveCam,
        Permalink $permalinkCam,
        array $thumbsCam,
        Nick $nickCam
    ): Cam {
        return Cam::instantiate(
            $idCam,
            $liveCam,
            $permalinkCam,
            $thumbsCam,
            $nickCam
        );
    }

    /**
     * @return Cam
     * @throws Exception
     */
    public static function randomLiveCam(): Cam
    {
        return self::create(
            IdMother::random(),
            LiveMother::isLive(),
            PermalinkMother::random(),
            [
                ThumbMother::random(),
                ThumbMother::random(),
                ThumbMother::random(),
                ThumbMother::random(),
            ],
            NickMother::random()
        );
    }

    /**
     * @return Cam
     * @throws Exception
     */
    public static function randomNotLiveCam(): Cam
    {
        return self::create(
            IdMother::random(),
            LiveMother::isNotLive(),
            PermalinkMother::random(),
            [
                ThumbMother::random(),
                ThumbMother::random(),
                ThumbMother::random(),
                ThumbMother::random(),
            ],
            NickMother::random()
        );
    }
}
