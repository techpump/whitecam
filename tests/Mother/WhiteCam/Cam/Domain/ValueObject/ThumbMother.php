<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject;

use Faker\Factory;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Thumb;
use Exception;

final class ThumbMother
{
    /**
     * @param string $thumb
     * @return Thumb
     * @throws Exception
     */
    public static function create(string $thumb): Thumb
    {
        return Thumb::fromString($thumb);
    }

    /**
     * @return Thumb
     * @throws Exception
     */
    public static function random(): Thumb
    {
        $faker = Factory::create();
        return self::create($faker->asciify('*********************************' . '.jpg'));
    }
}
