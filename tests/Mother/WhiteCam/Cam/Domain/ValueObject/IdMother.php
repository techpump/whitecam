<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject;

use Faker\Factory;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Id;
use Exception;

final class IdMother
{
    /**
     * @param string $id
     * @return Id
     * @throws Exception
     */
    public static function create(string $id): Id
    {
        return Id::fromString($id);
    }

    /**
     * @return Id
     * @throws Exception
     */
    public static function random(): Id
    {
        $faker = Factory::create();
        return self::create((string) $faker->randomNumber());
    }
}
