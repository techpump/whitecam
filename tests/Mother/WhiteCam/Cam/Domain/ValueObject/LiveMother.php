<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject;

use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Live;
use Exception;

final class LiveMother
{
    /**
     * @param string $live
     * @return Live
     * @throws Exception
     */
    public static function create(string $live): Live
    {
        return Live::fromString($live);
    }

    /**
     * @return Live
     * @throws Exception
     */
    public static function isLive(): Live
    {
        return self::create((string) 1);
    }

    /**
     * @return Live
     * @throws Exception
     */
    public static function isNotLive(): Live
    {
        return self::create((string) 0);
    }
}
