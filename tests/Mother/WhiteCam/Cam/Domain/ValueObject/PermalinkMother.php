<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Mother\WhiteCam\Cam\Domain\ValueObject;

use Faker\Factory;
use TECHPUMP\WhiteCam\Cam\Domain\ValueObject\Permalink;
use Exception;

final class PermalinkMother
{
    /**
     * @param string $permalink
     * @return Permalink
     * @throws Exception
     */
    public static function create(string $permalink): Permalink
    {
        return Permalink::fromString($permalink);
    }

    /**
     * @return Permalink
     * @throws Exception
     */
    public static function random(): Permalink
    {
        $faker = Factory::create();
        return self::create(strtolower($faker->firstName));
    }
}
