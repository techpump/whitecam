<?php

declare(strict_types=1);

namespace TECHPUMP\Tests\Mother\WhiteCam\Cam\Application\Command;

use TECHPUMP\WhiteCam\Cam\Application\Command\GetCamList;

class GetCamListMother
{
    /**
     * @return GetCamList
     */
    public static function create(): GetCamList
    {
        return new GetCamList();
    }
}