# WhiteCam
Proyecto para dar un servicio de webcams de marca blanca, de cara a que cualquier cliente pueda utilizarlo son la información de su empresa.

## Desarrollo local

### Servidor local Vagrant

Descargar el proyecto localWhiteCam y seguir las instrucciones para poder trabajar en local con el proyecto

### Uso del proyecto

Una vez se haya realizado la instalación del proyecto localWhiteCam, se podrán realizar peticiones en las siguientes direcciones:

* http://whitecam.test : se ha creado un sitio ficticio sin configuraciones, para que pueda verse la excepción que lanza si no está configurado en el proyecto.
* http://www.cerdas.com : se ha creado un site que consume el servicio de webcams de WhiteCam para el cliente "Cerdas".
* http://www.babosas.biz : se ha creado un site que consume el servicio de webcams de WhiteCam para el cliente "Babosas".
* http://www.ConejoX.com : se ha creado un site que consume el servicio de webcams de WhiteCam para el cliente "ConejoX".

Cada uno de los sites, tiene la lista completa de webcams que nos ofrece Cumlouder desde: http://webcams.cumlouder.com/feed/webcams/online/61/1/.

Si queremos realizar un cambio en la petición, podemos hacerlo desde el .env del proyecto. Por ejemplo, http://webcams.cumlouder.com/feed/webcams/online/30/1/

Las peticiones tienen un tiempo de caché de 60 segundos. Habría que configurarlo al tiempo que queramos que se mantenga la lista de webcams cacheada. Se ha configurado este tiempo por motivos de agilidad a la hora de probar.

Este valor también es configurable desde el archivo .env, modificando la variable CACHE_EXPIRATION_TIME. Si se configura a 0, el proyecto no utilizará caché.

### Cómo se ha desarrollado

Para el desarrollo del proyecto se ha utilizado arquitectura hexagonal. El servicio se ha implementado desde los test, utilizando el patrón TDD y para la organización de
los test se ha utilizado el patrón Object Mother. Siempre se ha intentado mantener buenas prácticas, clean code y respetar los principios SOLID. Se han realizado las implementaciones 
de manera que sea un código autocomentado, para intentar mejorar la mantenibilidad del proyecto en un futuro.

Se ha utilizado el proyecto de Homestead de Laravel como entorno local, ya que tiene muchísima flexibilidad y bastantes herramientas relacionadas con el desarrollo en PHP muy
sencillas de usar. 

Además, gracias a la flexibilidad que ofrece, se ha desarrollado un Makefile con algunos comandos de utilidad cuando el entorno está desplegado: lanzar phpunit, 
generar el informe de coverage, realizar composer update si fuera necesario, etc. Se pueden ver todos los comandos que hay lanzando el comando `make help` desde la carpeta 
de entorno local que se debe clonar.

> IMPORTANTE!
>
> Para simular el servicio CNAME de cada cliente, la instalación del proyecto local añade en el /etc/hosts una serie de direcciones para apuntar los clientes especificados al entorno local. Una vez finalizado el 
> desarrollo, es conveniente eliminar dichos registros para evitar problemas.


Como IDE se ha utilizado PHPStorm 2020.1.3 sobre Ubuntu y las pruebas se han realizado desde Google Chrome. La organización del proyecto en base a la especificación y las dudas resueltas se ha hecho en Trello, 
separando las tareas y asignando puntos de historia, creando así un sprint de una semana completa. Finalmente las horas aproximadas de trabajo han sido 24 de 26 planificadas, aproximadamente.

Se ha utilizado PHP 7.4.5, el cual viene genial para este proyecto debido a las mejoras de rendimiento que tiene respecto a las versiones anteriores.

Se ha utilizado PHPUNIT 9.2.5 y la versión de XDebug 2.9.3 para el desarrollo y lanzamiento de los test en local.

Las tareas completadas han sido mergeadas a Master desde Gitlab, de manera que cada uno de los merge request queda recogido en el proyecto.

Para una hipotética salida a producción, bastaría con sacar una release del proyecto y ajustar tiempos de caché.

### Análisis del proyecto

Se describirá un breve análisis de los puntos clave del proyecto en este apartado.

#### Comentarios y defensa

* Se ha incluido la llamada al servicio de webcams en el .env del proyecto, de manera que pueda modificarse para obtener más o menos webcams.

* Se puede deshabilitar el sistema de caché también desde el .env, añadiendo como tiempo de caché 0 en la variable correspondiente.

* Los archivos de configuración pertenecientes a cada uno de los clientes se encuentran en config/clients/prod/[nombre del site]

* Si el servicio de caché no devuelve webcams por algún motivo, el cliente es redirigido a https://webcams.cumlouder.com/404/. Esta URL de redirección es personalizable
en la configuración de cada cliente, modificando la variable `client_404_redirect` del archivo de configuración.

* Se ha realizado una implementación de caché utilizando el sistema de caché de Symfony, por rapidez y por sencillez. Si se quisiera utilizar algún otro tipo de adaptador, 
el sistema está preparado para realizar la implementación de un Redis, o quizá un MinIO si el despliegue en producción se va a realizar sobre Kubernetes, ya que tiene un sistema 
de lectura optimizado para este tipo de despliegue. Además, desde la máquina de la que se sirvan peticiones de WhiteCam, se deberá activar opCache para optimizar la velocidad de consulta.

* Actualmente, se puede ver el coverage del proyecto con el comando `make phpunit-coverage` y entrando al index que se genera en public/coverage desde http://localhost:63342/whiteCam/public/coverage/index.html en el navegador

#### Puntos calientes del proyecto

Los puntos en los que me he visto más incómodo a la hora de resolver el problema han sido:

* A la hora de generar la vista. La organización de los arrays para que desde la vista se procese lo menos posible ha sido un punto que me ha llevado más tiempo del esperado resolver.

* Desde los test, leer de caché. Para completar el coverage del proyecto, he creado un test en el que se llama dos veces al mismo servicio, de manera que la primera escribe en caché y la segunda lo lee desde ella.

#### Tareas pendientes

A pesar de haber intentado realizar el desarrollo lo más completo posible, dejo unos apuntes que creo que serían necesarios para completar el proyecto.

* Realización de test funcionales para probar el EntryPoint del cliente

* Añadir un sistema de log, ya que en el desarrollo no ha sido necesario (por el uso de xDebug) pero de cara a un entorno real, es imprescindible.

* Pasar un analizador de código. En mi caso, suelo utilizar psalm, bastante potente y muy cómodo, está en el composer.json. La URL es https://psalm.dev/

* Hacer algunas pruebas ab, para ver la respuesta que tienen las peticiones aunque sea a nivel local, para ver que realmente con caché mejoran los tiempos. No se han realizado principalmente por tiempo.

### Conclusiones

Me he sentido cómodo al realizar la prueba. No es un servicio difícil de implementar y en lo único que he visto quizá un poco de problemática ha sido en los puntos calientes mencionados un poco más arriba en este 
mismo documento.

Con la resolución de las dudas pude realizar un análisis correcto y estimar bien los tiempos de trabajo, por lo que no me ha sido complicado llegar a la entrega y tampoco me ha hecho falta dedicar horas de más a la 
prueba.

En cualquier caso, si creo que puedo aportar algo más al documento, actualizaré el Readme, aunque creo que está toda la información.

Ha sido un placer, espero que sea un código de calidad y que merezca la pena ;)


